package u06lab.code

/**
  * 1) Implement trait Functions with an object FunctionsImpl such that the code
  * in TryFunctions works correctly.
  *
  * 2) To apply DRY principle at the best,
  * note the three methods in Functions do something similar.
  * Use the following approach:
  * - find three implementations of Combiner that tell (for sum,concat and max) how
  *   to combine two elements, and what to return when the input list is empty
  * - implement in FunctionsImpl a single method combiner that, other than
  *   the collection of A, takes a Combiner as input
  * - implement the three methods by simply calling combiner
  *
  * When all works, note we completely avoided duplications..
 */

trait Functions {
  def sum(a: List[Double]): Double
  def concat(a: Seq[String]): String
  def max(a: List[Int]): Int
}

trait Combiner[A] {
  def unit: A
  def combine(a: A, b: A): A
}

object FunctionsImpl extends Functions {

  override def sum(a: List[Double]): Double = a match {
    case List() => 0
    case _ => a.head + sum(a.tail)
  }

  override def concat(a: Seq[String]): String = a match {
    case Seq() => ""
    case _ => a.head + concat(a.tail)
  }

  override def max(a: List[Int]): Int = a match {
    case List() => Int.MinValue
    case _ =>
      var max = a.head
      a.foreach(e => max = if (e > max) e else max)
      max
  }

  def sumCombined[T: Combiner](a: List[T]): T = a match {
    case List() => implicitly[Combiner[T]].unit
    case _ => implicitly[Combiner[T]].combine(a.head, sumCombined(a.tail))
  }

  def concatCombined[T: Combiner](a: Seq[T]): T = a match {
    case Seq() => implicitly[Combiner[T]].unit
    case _ => implicitly[Combiner[T]].combine(a.head, concatCombined(a.tail))
  }

  def maxCombined[T: Combiner](a: List[T]): T = {
    var max = implicitly[Combiner[T]].unit
    a.foreach(e => max = if (implicitly[Combiner[T]].combine(e, max).toString > max.toString) e else max)
    max
  }
}

object ImplicitCombiners {

  implicit object StringCombiner extends Combiner[String] {
    override def combine(a: String, b: String): String = a concat b

    override def unit: String = ""
  }

  implicit object IntCombiner extends Combiner[Int] {
    override def combine(a: Int, b: Int): Int = a + b

    override def unit: Int = Int.MinValue
  }

  implicit object DoubleCombiner extends Combiner[Double] {
    override def combine(a: Double, b: Double): Double = a + b

    override def unit: Double = 0
  }

}

object TryFunctions extends App {
  val f: Functions = FunctionsImpl
  val fc = FunctionsImpl
  println("Not combined.")
  println(f.sum(List(10.0, 20.0, 30.1))) // 60.1
  println(f.sum(List())) // 0.0
  println(f.concat(Seq("a", "b", "c"))) // abc
  println(f.concat(Seq())) // ""
  println(f.max(List(-10, 3, -5, 0))) // 3
  println(f.max(List())) // -2147483648

  println("Combined.")
  import ImplicitCombiners._
  println(fc.sumCombined(List(10.0, 20.0, 30.1))) // 60.1
  // println(fc.sumCombined(List())) // 0.0
  println(fc.concatCombined(Seq("a", "b", "c"))) // abc
  // println(fc.concatCombined(Seq())) // ""
  println(fc.maxCombined(List(-10, 3, -5, 0))) // 3
  // println(fc.maxCombined(List())) // -2147483648
}